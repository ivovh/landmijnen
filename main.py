# Rules:
# 1. grid of 6x6. all cards turned face down. Each player has three lives. DONE
# 2. four bombs (DONE)
# 2.b. Bombs are placed by previous player (can move bomb if you land on it) DONE
# 3. throw two dice DONE
# 4. dice determine the co-ordinates (1st vertical, 2nd horizontal) DONE
# 5. flip the card and lose a life if there is a bomb on the card DONE
# 6. Player who has no life left loses DONE
#
# Optional features:
# a. One bomb on multiple cards (like on a roulette table)
#   * Check if placement is actually possible (maximum 1 place difference in coordinate)
#   * Prevent placing on 3 cards instead of 1, 2 or 4
#   * Adapt rest of the program to new bomb list
# b. Physically modelling the rolling of the die

import webbrowser, keyboard
from string import Template
from playsound import playsound
import random, time

NUMBER_OF_BOMBS = 4
NUMBER_OF_ROWS = 6
NUMBER_OF_COLUMNS = 6
START_LIVES_OF_PLAYER = 3
MAX_NUMBER_OF_PLAYERS = 10

BOMB_CARD = '💣'
EMPTY_CARD = ' '
CLOSED_CARD = '🎴'
DIE_FACES = ['1️⃣', '2️⃣', '3️⃣', '4️⃣', '5️⃣', '6️⃣']

BOMB_SOUND = 'bomb.mp4'
SAD_SOUND = 'sad.mp4'

def generate_html(gameboard):
    """Generate HTML for the game board"""
    display_gameboard = {}
    for card_name, value in gameboard.items():
        display_gameboard[card_name] = get_symbol_for_card(value)

    with open('template.html', 'r') as template_file:
        with open('output.html', 'w') as output_file:
            my_template = Template(template_file.read())
            output_string = my_template.substitute(display_gameboard)
            output_file.write(output_string)

    safari = webbrowser.get('safari')
    safari.open('file:///Users/ihu03/src/coding-sessions/landmijnen/output.html')

    # time.sleep(5)
    # keyboard.press_and_release("ctrl+w")

def print_gameboard(gameboard):
    """Print the gameboard"""
    current_column = 0

    for position,value in gameboard.items():
        symbol = get_symbol_for_card(value)

        print(symbol + ' ', end='')
        if current_column == NUMBER_OF_COLUMNS-1:
            print()
            current_column = 0
        else:
            current_column += 1

    print()

def get_symbol_for_card(card):
    if not card['flipped']:
        return CLOSED_CARD
    elif card['bomb']:
        return BOMB_CARD
    else:
        return EMPTY_CARD

def print_players(players):
    """Print players and remaining lives"""
    for index, player in enumerate(players):
        print(f"Player {index+1}: {player['name']} ({player['lives']} lives)")
    print()

def setup_board():
    """Set up the board for a new game"""
    gameboard = {}
    for row in range(1, NUMBER_OF_ROWS+1):
        for column in range(1, NUMBER_OF_COLUMNS+1):
            gameboard[f"card{row}_{column}"] = {'flipped': False}
    
    # Place bombs
    bombs = []
    for bomb in range(0,NUMBER_OF_BOMBS):
        bomb_has_been_placed = False

        while not bomb_has_been_placed:
            random_row = random.randint(1,6)
            random_column = random.randint(1,6)
            bomb_has_been_placed = place_bomb(bombs, [{row: random_row, column: random_column}])
    return (gameboard, bombs)


def setup_players():
    """Ask users for names and number of players"""
    while True:
        number_of_players = input(f"How many players are playing? [2-{MAX_NUMBER_OF_PLAYERS}] ")
        if number_of_players.isdigit() and int(number_of_players) in range(2, MAX_NUMBER_OF_PLAYERS+1):
            number_of_players = int(number_of_players)
            break
        else:
            print("Not a valid number :(")
    players = []
    for x in range(number_of_players):
        while True:
            name = input(f"What is the name of player {x+1}? ")
            name = name.strip()
            if name != '':
                break
            else:
                print("Please enter a name!")
        players.append({"name": name, "lives": START_LIVES_OF_PLAYER})
    return players

def throw_die(die_number):
    "Throw a die and show the result"
    input(f"Press return to throw the {die_number} die")
    for x in range(10):
        die_result = random.randint(1,6)
        print("\r" + DIE_FACES[die_result-1] + "  ", end='')
        time.sleep(0.1)
    print()
    print("You threw: " +  str(die_result))
    return die_result

def place_bomb(bombs, new_bomb_positions):
    """Place a bomb on the specified location
    bomb: list of existing bombs
    new_bomb_positions: list of coordinates for the new bomb"""
    # Check if specified row and column already have bomb
    for bomb in bombs:
        for coordinate in bomb:
            # [{row: 1, column: 2}, {row: 3, column: 4}]
            for new_position in new_bomb_positions:
                if new_position.row == coordinate.row and new_position.column == coordinate.column:
                    return False
    # Bomb was not found at specified position, so place bomb
    bombs.append(new_bomb_positions)
    return True

def player_moves_bomb(gameboard):
    """Let a player move a bomb to a new location"""
    bomb_has_been_placed = False
    print("Move this bomb to a new location.")

    amount_of_cards_covered = 0
    while amount_of_cards_covered <= 4: # TODO check of het 3 is
        # Place bomb on one card
        while not bomb_has_been_placed:
            chosen_row = input("Choose a row: ")
            if chosen_row.isdigit() and int(chosen_row) in range(1, NUMBER_OF_ROWS+1):
                # Continue with the rest of the function
                pass
            else:
                print("Not a valid row!")
                continue
            chosen_column = input("Choose a column: ")
            if chosen_column.isdigit() and int(chosen_column) in range(1, NUMBER_OF_COLUMNS+1):
                # Continue with the rest of the function
                pass
            else:
                print("Not a valid row!")
                continue

            bomb_has_been_placed = place_bomb(gameboard, chosen_row, chosen_column)
            if not bomb_has_been_placed:
                print("There is already a bomb here! Choose a new location.")

        amount_of_cards_covered = amount_of_cards_covered + 1
        if amount_of_cards_covered == 3:

        another_card = input("Do you want to place it on an neighbouring card? (y/n) ")
        if another_card != 'y':
            break

if __name__ == "__main__":
    # Generate the game board
    gameboard = setup_board() # TODO returnt nu ook bombs
    
    # Set up the players
    players = setup_players()
    number_of_players = len(players)

    # Play a turn
    current_player = 0;
    while len(players) > 1:
        print()
        print_players(players)
        print_gameboard(gameboard)
        generate_html(gameboard)

        print(f"Player {current_player+1}'s turn!")
        row = throw_die('first')
        column = throw_die('second')

        key = f"card{row}_{column}"
        print(f"Card at [{row},{column}] flipped")
        gameboard[key]['flipped'] = True
        if gameboard[key]['bomb']:
            generate_html(gameboard)
            print("💣💣💣 It's a BOMB!! 💥💥💥")
            playsound(BOMB_SOUND)
            players[current_player]["lives"] -= 1
            if players[current_player]["lives"] == 0:
                # Game over for current player
                print("You just died! 💀")
                playsound(SAD_SOUND)
                players.pop(current_player)
            else:
                # Player lives and may move the bomb
                player_moves_bomb(gameboard)
                gameboard[key]['flipped'] = False


        current_player = (current_player + 1) % number_of_players
    
    print(f"🎉 Congratulations!!! You won {players[0]['name']}!")
